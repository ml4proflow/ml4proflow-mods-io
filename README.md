# ml4proflow-mods-io

This package provides basic input/output modules for ml4proflow.

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/file/reports/junit/report.html?job=gen-cov)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/file/reports/coverage/index.html?job=gen-cov)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/file/reports/flake8/index.html?job=gen-cov)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)]()
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)]()
------------
## Installation
Activate your virtual environment (optionally) and
install the package with pip:
```bash 
pip install ml4proflow-mods-io
```

## Contribution
For development, install this repository in editable mode with pip:
```bash 
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io.git
cd ml4proflow-mods-io
pip install -e .
```

